# Cloud Resume

My resume developed and hosted using cloud technologies. Inspired by the [Cloud Resume Challenge](https://cloudresumechallenge.dev/)

## Writeup

See my breakdown of the challenge in this [writeup document](writeup.markdown)
