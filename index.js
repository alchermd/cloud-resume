window.onload = () => {
	const API_URL = "https://wghtagv1b1.execute-api.us-east-1.amazonaws.com/prod/incrementsitehits";
	fetch(API_URL)
		.then(res => res.json())
		.then(data => {
			const visitorCount = document.getElementById('view-count');
			visitorCount.setAttribute("title", `This page has been viewed ${data.body.Hits} times`);
		})
}

