My notes on how I completed the steps for the [Cloud Resume Challenge](https://cloudresumechallenge.dev/)

## Step 1 - AWS Cloud Practitioner Certificate

With the Corona situation still on going, I wasn't able to take the exam. Hopefully things get better soon!

## Step 2 - HTML

I tried to apply [semantic HTML](https://developer.mozilla.org/en-US/docs/Glossary/Semantics#Semantics_in_HTML) as much as I can. That means appropriately using header tags, using `section`, `nav`, and `main` instead of `div`s, and proper usage of `table` elements. The end result is quite barebones and not easy on the eyes, but that's for the next step...

![](assets/02-html.png)

## Step 3 - CSS

As a software engineer that works daily within REST APIs and backend systems, my UI and design skills are embarassingly left to be desired. My go-to solution when building a frontend would be to reach for [Bootstrap](https://getbootstrap.com/) -- it is battle-tested and the next developer to work on my code could easily decipher what I'm trying to do. But for a single static page, I feel that Bootstrap would be overkill. So I decided to use a _classless_ CSS library, wherein the one I picked is [water.css](https://watercss.kognise.dev/). How these libaries work is by applying sane default styling to an HTML document, removing the need to apply CSS classes to my HTML elements. Having used semantic HTML from the previous step, water.css gave me a perfect looking page out of the box. I added a couple of style rules to control certain parts such as the navbar, but aside from that, my resume is ready for the cloud!

![](assets/03-css.png)

## Step 4 - Static S3 Website

Having used [S3](https://aws.amazon.com/s3/) on a day to day basis, this step is pretty much a cinch to complete. All I did was:

1. Create an S3 bucket
2. Allow public access to it
3. Configure it for website hosting
4. Drag and drop my `index.html` and `index.css` files
5. And I'm done!

I now have a publicly viewable web page, but that red "unsecured" padlock icon is glaring at me...

## Step 5 - HTTPS

[CloudFront](https://aws.amazon.com/cloudfront/) is a service that I haven't used before. But how hard can it be? It's just a CDN that could allow me to host my assets (in this case my S3 website) with HTTPS. So I went ahead and tried creating my first CloudFront distribution in the console... and my account is "not verified" and would need to get in touch with the AWS service team.

So I did just that. After 3 days, I finally get a confirmation that my AWS account is ready to spin up new CloudFront distributions. I tried following the creation wizard and chose my S3 bucket from the choices for the **Origin Domain**. I waited for the distribution to be deployed, visited my S3 site with HTTPS, and... it fails. 

Apprently, I have to use the actual S3 Website URL instead of the Bucket itself. So I updated my CloudFront config, saved the changes, and went ahead and checked my S3 Website URL with HTTPS this time. Of course, it still fails.

That's when it dawned on me: The S3 Website URL itself won't be affected, I have to use the generated URL from CloudFront to access my site! So I went to `https://mycloudfrontdistro.cloudfront.net`, and what do you know, my site is being served correctly!

## Step 6 - DNS

Having an existing domain hosted in [Vercel](https://vercel.com/), I wanted to reuse the same domain and add a subdomain like `cv.alcher.dev` for this cloud resume. Unfortunately, Route53 and Vercel's DNS manager doesn't play well together. So I [migrated my whole website/blog to AWS](https://alcher.dev/2020/jekyll-blog-vercel-to-aws/). This eases up the process A TON, so if it's applicable to you, you might want to consider doing the same.

Configuring the subdomain is pretty simple at this point:

1. Under [Certificate Manager](https://console.aws.amazon.com/acm/home), create a certificate for `cv.alcher.dev`.
2. In the verification process, accept the prompt to create a `CNAME` record in Route53.
3. Back in Route53, define a simple record for `cv.alcher.dev`.
4. Add an `A` record, pointing to the CloudFront distribution.

We're now done! Visiting [cv.alcher.dev](https://cv.alcher.dev/) will now server the contents of our CloudFront distribution.

## Step 7 - Javascript

The goal is to create a view counter that tracks how many hits did the page had accumulated. This is pretty much impossible with JavaScript alone, so what I did is layout the foundation on what structure might I need once I got a backend storage set up. Here's what I came up:

```js
window.onload = () => {
	const getUpdatedCount = () => {
		// TODO: Integrate with a backend API.
		return 1;
	}

	const viewCount = document.getElementById('view-count');
	viewCount.setAttribute("title", `This page has been viewed ${getUpdatedCount()} times`);
}
```

Basically, once the page is ready, I'd run a function to update the view count. This function will presumably make network calls to a backend service, but for now it just returns `1`.

## Step 8 - Database

After reading up on how [DynamoDB](https://aws.amazon.com/dynamodb/) works, I decided to use the simplest implementation that I can think of: A table of `Sites` with columns for `Url` and `Hits`. I went ahead and created the `Sites` table and inserted an initial entry for my resume site:

```json
{
	"Sites": [
		{
			"Url": "cv.alcher.dev",
			"Hits": 1,
		}
	]
}
```

## Step 9 - API

As prescribed by the challenge, instead of accessing DynamoDB through JavaScript directly, I created an API with [Lambda](https://aws.amazon.com/lambda/) and [API Gateway](https://aws.amazon.com/api-gateway/). Specifically, I built a REST API integrated with a Python Lambda function, with CORS enabled for my resume site. At this point, my previous JS code will need some tweaking as we could see in the Python section:

```js
window.onload = () => {
	const API_URL = "https://api-gateway-endpoint/;
	fetch(API_URL)
		.then(res => res.json())
		.then(data => {
			const visitorCount = document.getElementById('view-count');
			visitorCount.setAttribute("title", `This page has been viewed ${data.body.Hits} times`);
		})
}
```

## Step 10 - Python

Top top things off, I used Python and the [boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html) library to write the Lambda function that interacts with DynamoDB. After reading up on the `boto3` DynamoDB docs, the Lambda script is actually quite straightforward:

```py
import json
import boto3

def lambda_handler(event, context):
    table = boto3.resource('dynamodb').Table('Sites')
    cv_key = {'Url': 'cv.alcher.dev'}
    
    # Increment the current hits value
    table.update_item(Key=cv_key, AttributeUpdates={"Hits": {"Value": 1, "Action": "ADD"}})
    
    # Fetch the updated hits
    item = table.get_item(Key=cv_key)["Item"]
    body = {**item, "Hits": int(item.get("Hits", 0))}
    
    return {
        'statusCode': 200,
        "body": body
    }
```

## step 11 - tests

> wip

## step 12 - infrastructure as code

> wip

## step 13 - source control

> wip

## step 14 - ci/cd (backend)

> wip

## step 15 - ci/cd (frontend)

> wip

## step 16 - blog post

> wIP


